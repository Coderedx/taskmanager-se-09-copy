package com.shumov.tm;

import com.shumov.tm.bootstrap.Bootstrap;
import com.shumov.tm.command.AbstractCommand;
import com.shumov.tm.command.entity.project.*;
import com.shumov.tm.command.entity.task.*;
import com.shumov.tm.command.entity.user.*;
import com.shumov.tm.command.help.AboutCommand;
import com.shumov.tm.command.help.ExitCommand;
import com.shumov.tm.command.help.HelpCommand;
import org.jetbrains.annotations.NotNull;
import org.reflections.Reflections;

import java.util.Set;

public class App
{
//    private static final Class[] CLASSES = {
//            HelpCommand.class, UserLoginCommand.class, UserRegCommand.class,
//
//            ProjectClearCommand.class, ProjectCreateCommand.class, ProjectEditCommand.class,
//            ProjectListCommand.class, ProjectRemoveCommand.class, ProjectTasksCommand.class,
//
//            TaskClearCommand.class, TaskCreateCommand.class, TaskEditCommand.class,
//            TaskListCommand.class, TaskRemoveCommand.class, TaskAddProjectCommand.class,
//
//            ProjectAdminClearDBCommand.class, ProjectAdminEditCommand.class, ProjectAdminListCommand.class,
//            ProjectAdminRemoveCommand.class, ProjectAdminTasksCommand.class, TaskAdminAddProjectCommand.class,
//            TaskAdminClearDBCommand.class, TaskAdminEditCommand.class, TaskAdminListCommand.class,
//            TaskAdminRemoveCommand.class, UserAdminRegCommand.class, UserAdminReviewCommand.class,
//
//            UserEditCommand.class, UserReviewCommand.class, UserUpdPassCommand.class,
//            UserCloseSessionCommand.class, AboutCommand.class, ExitCommand.class
//    };

    // Не сохраняется порядок команд
    @NotNull
    private static final Set<Class<? extends AbstractCommand>> classes =
            new Reflections("com.shumov.tm").getSubTypesOf(AbstractCommand.class);

    public static void main( String[] args )
    {
        @NotNull Bootstrap bootstrap = new Bootstrap();
        try {
            System.out.println("TEST");
            System.out.println("TEST");
            System.out.println("TEST");
            bootstrap.init(classes);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
