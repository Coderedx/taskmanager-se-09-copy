package com.shumov.tm.api.entity;

import org.jetbrains.annotations.NotNull;

public interface Entity {

    @NotNull
    String getId();
    void setId(@NotNull final String id);

    @NotNull
    String getOwnerId();
    void setOwnerId(@NotNull final String ownerId);

}
