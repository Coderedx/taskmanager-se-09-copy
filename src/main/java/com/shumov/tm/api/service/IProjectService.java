package com.shumov.tm.api.service;

import com.shumov.tm.entity.Project;
import com.shumov.tm.enumerate.ExecutionStatus;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;

public interface IProjectService extends Service {

    void createProject(@Nullable final String ownerId, @Nullable final String name) throws Exception;

    @NotNull
    List<Project> getProjectList() throws Exception;

    @NotNull
    List<Project> getProjectList(@Nullable final String ownerId) throws Exception;

    @NotNull
    Project getProject(@Nullable final String id) throws Exception;

    @NotNull
    Project getProject(@Nullable final String ownerId, @Nullable final String id) throws Exception;

    void editProjectNameById(@Nullable final String id, @Nullable final String name) throws Exception;

    void editProjectNameById(@Nullable final String ownerId,
                             @Nullable final String id,
                             @Nullable final String name) throws Exception;

    void editProjectDate(@Nullable final String ownerId,
                         @Nullable final String id,
                         @Nullable final String start,
                         @Nullable final String finish) throws Exception;

    public void editProjectStatus(@Nullable String ownerId,
                                  @Nullable String id,
                                  @NotNull ExecutionStatus status) throws Exception;

    void removeProjectById(@Nullable final String id,
                           @NotNull final ITaskService taskService) throws Exception;

    void removeProjectById(@Nullable final String ownerId,
                           @Nullable final String id,
                           @NotNull final ITaskService taskService) throws Exception;

    void clearData(@NotNull final ITaskService taskService) throws Exception;

    void clearData(@Nullable final String ownerId,
                   @NotNull final ITaskService taskService) throws Exception;

    @NotNull ExecutionStatus projectStatus(@NotNull final String status) throws Exception;
}
