package com.shumov.tm.api.service;

import org.jetbrains.annotations.NotNull;

public interface ITerminalService extends Service {
    @NotNull
    String nextLine();
}
