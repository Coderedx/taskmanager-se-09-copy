package com.shumov.tm.api.service;

import com.shumov.tm.entity.User;
import com.shumov.tm.enumerate.UserRoleType;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.IOException;

public interface IUserService extends Service {

    @NotNull
    User getUser(@Nullable final String login,
                 @Nullable final String password) throws Exception;

    void mergeUser(@Nullable final User user) throws Exception;

    void createNewUser(@Nullable final String login,
                       @Nullable final String password) throws Exception;

    void createNewUser(@Nullable final String login,
                       @Nullable final String password,
                       @NotNull final UserRoleType userRole) throws Exception;

    void isCorrectLogin(@Nullable final String login) throws IOException;

    void isCorrectPass(@Nullable final String pass) throws IOException;

    void isCorrectDescription(@Nullable final String description) throws IOException;
}
