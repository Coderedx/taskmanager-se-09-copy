package com.shumov.tm.command.entity.project;

import com.shumov.tm.api.service.IProjectService;
import com.shumov.tm.api.service.ITerminalService;
import com.shumov.tm.command.AbstractCommand;
import com.shumov.tm.entity.Project;
import com.shumov.tm.entity.User;
import com.shumov.tm.enumerate.UserRoleType;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

public class ProjectSearchCommand extends AbstractCommand {

    @Override
    public @NotNull String command() {
        return "project-search";
    }

    @Override
    public @NotNull String getDescription() {
        return "Project search";
    }

    @Override
    public void execute() throws Exception {
        if(serviceLocator == null) return;
        System.out.println("[PROJECT SEARCH]");
        System.out.println("enter a search term:".toUpperCase());
        @NotNull final ITerminalService terminalService = serviceLocator.getTerminalService();
        @NotNull final String term = terminalService.nextLine();
        @NotNull final IProjectService projectService = serviceLocator.getProjectService();
        projectService.isCorrectInputData(term);
        @NotNull final User user = serviceLocator.getCurrentUser();
        @NotNull final List<Project> listProjects = projectService.getProjectList(user.getId());
        @NotNull final List<Project> listResult = new ArrayList<>();
        for (@NotNull final Project project : listProjects) {
            if(project.getName().contains(term) || project.getDescription().contains(term)){
                listResult.add(project);
            }
        }
        System.out.println("[SEARCH RESULT]");
        for (@NotNull final Project project : listResult) {
            System.out.println("PROJECT ID: " + project.getId() + " PROJECT NAME: " + project.getName() +
                    "\nDATE CREATE: " + formatter.format(project.getDateCreated()) +
                    "\nDATE START: " + formatter.format(project.getDateStart()) +
                    "\nDATE FINISH: " + formatter.format(project.getDateFinish()) +
                    "\nSTATUS: " + project.getStatus().toString()+"\n");
        }
    }

    @Override
    public void initRoles() {
        roleTypes.add(UserRoleType.USER);
        roleTypes.add(UserRoleType.ADMIN);
    }
}
