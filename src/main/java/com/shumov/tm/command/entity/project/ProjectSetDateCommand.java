package com.shumov.tm.command.entity.project;

import com.shumov.tm.api.service.IProjectService;
import com.shumov.tm.api.service.ITerminalService;
import com.shumov.tm.command.AbstractCommand;
import com.shumov.tm.entity.User;
import com.shumov.tm.enumerate.UserRoleType;
import org.jetbrains.annotations.NotNull;

public class ProjectSetDateCommand extends AbstractCommand {

    @Override
    public @NotNull String command() {
        return "project-set-date";
    }

    @Override
    public @NotNull String getDescription() {
        return "Project set date";
    }

    @Override
    public void execute() throws Exception {
        if(serviceLocator == null) return;
        System.out.println("[PROJECT SET DATE]");
        System.out.println("ENTER PROJECT ID:");
        @NotNull final ITerminalService terminalService = serviceLocator.getTerminalService();
        @NotNull final String id = terminalService.nextLine();
        System.out.println("ENTER START DATE dd.MM.yyyy:");
        @NotNull final String dateStart = terminalService.nextLine();
        System.out.println("ENTER FINISH DATE dd.MM.yyyy:");
        @NotNull final String dateFinish = terminalService.nextLine();
        @NotNull final IProjectService projectService = serviceLocator.getProjectService();
        @NotNull final User user = serviceLocator.getCurrentUser();
        projectService.editProjectDate(user.getId(), id, dateStart, dateFinish);
        System.out.println("[OK]");
    }

    @Override
    public void initRoles() {
        roleTypes.add(UserRoleType.USER);
        roleTypes.add(UserRoleType.ADMIN);
    }
}
