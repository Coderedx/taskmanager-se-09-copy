package com.shumov.tm.command.entity.project;

import com.shumov.tm.api.service.IProjectService;
import com.shumov.tm.api.service.ITerminalService;
import com.shumov.tm.command.AbstractCommand;
import com.shumov.tm.entity.User;
import com.shumov.tm.enumerate.ExecutionStatus;
import com.shumov.tm.enumerate.UserRoleType;
import org.jetbrains.annotations.NotNull;

public class ProjectSetStatus extends AbstractCommand {

    @Override
    public @NotNull String command() {
        return "project-set-status";
    }

    @Override
    public @NotNull String getDescription() {
        return "Set project status";
    }

    @Override
    public void execute() throws Exception {
        if(serviceLocator == null) return;
        System.out.println("[PROJECT SET STATUS]");
        System.out.println("ENTER PROJECT ID:");
        @NotNull final ITerminalService terminalService = serviceLocator.getTerminalService();
        @NotNull final String id = terminalService.nextLine();
        System.out.println("ENTER PROJECT STATUS:");
        System.out.println("planned : project planned");
        System.out.println("progress : project in progress");
        System.out.println("done : project done");
        @NotNull final String status = terminalService.nextLine();
        @NotNull final IProjectService projectService = serviceLocator.getProjectService();
        @NotNull final User user = serviceLocator.getCurrentUser();
        @NotNull final ExecutionStatus taskStatus = projectService.projectStatus(status);
        projectService.editProjectStatus(user.getId(), id, taskStatus);
        System.out.println("[OK]");
    }

    @Override
    public void initRoles() {
        roleTypes.add(UserRoleType.USER);
        roleTypes.add(UserRoleType.ADMIN);
    }
}
