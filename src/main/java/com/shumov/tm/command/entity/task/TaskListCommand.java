package com.shumov.tm.command.entity.task;

import com.shumov.tm.api.service.ITaskService;
import com.shumov.tm.command.AbstractCommand;
import com.shumov.tm.entity.Task;
import com.shumov.tm.entity.User;
import com.shumov.tm.enumerate.UserRoleType;
import org.jetbrains.annotations.NotNull;

public class TaskListCommand extends AbstractCommand {

    @Override
    @NotNull
    public String command() {
        return "task-list";
    }

    @Override
    @NotNull
    public String getDescription() {
        return "Show all tasks";
    }

    @Override
    public void execute() throws Exception {
        userExecute();
    }

    @Override
    public void initRoles() {
        roleTypes.add(UserRoleType.USER);
        roleTypes.add(UserRoleType.ADMIN);
    }

    private void userExecute() throws Exception {
        if(serviceLocator == null) return;
        System.out.println("[TASK LIST]");
        @NotNull final User user = serviceLocator.getCurrentUser();
        @NotNull final ITaskService taskService = serviceLocator.getTaskService();
        for (@NotNull final Task task : taskService.getTaskList(user.getId())) {
            System.out.println("TASK ID: " + task.getId() + " TASK NAME: " + task.getName()
                    + " PROJECT ID: "+ task.getIdProject());
        }
    }
}
