package com.shumov.tm.command.entity.task;

import com.shumov.tm.api.service.ITaskService;
import com.shumov.tm.api.service.ITerminalService;
import com.shumov.tm.command.AbstractCommand;
import com.shumov.tm.entity.User;
import com.shumov.tm.enumerate.UserRoleType;
import org.jetbrains.annotations.NotNull;

public class TaskSetDateCommand extends AbstractCommand {

    @Override
    public @NotNull String command() {
        return "task-set-date";
    }

    @Override
    public @NotNull String getDescription() {
        return "Task set date";
    }

    @Override
    public void execute() throws Exception {
        if(serviceLocator == null) return;
        System.out.println("[TASK SET DATE]");
        System.out.println("ENTER TASK ID:");
        @NotNull final ITerminalService terminalService = serviceLocator.getTerminalService();
        @NotNull final String id = terminalService.nextLine();
        System.out.println("ENTER START DATE dd.MM.yyyy:");
        @NotNull final String dateStart = terminalService.nextLine();
        System.out.println("ENTER FINISH DATE dd.MM.yyyy:");
        @NotNull final String dateFinish = terminalService.nextLine();
        @NotNull final ITaskService taskService = serviceLocator.getTaskService();
        @NotNull final User user = serviceLocator.getCurrentUser();
        taskService.editTaskDate(user.getId(), id, dateStart, dateFinish);
        System.out.println("[OK]");
    }

    @Override
    public void initRoles() {
        roleTypes.add(UserRoleType.USER);
        roleTypes.add(UserRoleType.ADMIN);
    }
}
