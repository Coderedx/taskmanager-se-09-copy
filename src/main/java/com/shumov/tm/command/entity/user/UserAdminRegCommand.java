package com.shumov.tm.command.entity.user;

import com.shumov.tm.api.service.IUserService;
import com.shumov.tm.command.AbstractCommand;
import com.shumov.tm.enumerate.UserRoleType;
import org.jetbrains.annotations.NotNull;

public class UserAdminRegCommand extends AbstractCommand {

    @Override
    @NotNull
    public String command() {
        return "admin-reg";
    }

    @Override
    @NotNull
    public String getDescription() {
        return "New admin registration";
    }

    @Override
    public void execute() throws Exception {
        createAdmin();
    }

    @Override
    public void initRoles() {
        roleTypes.add(UserRoleType.ADMIN);
    }

    private void createAdmin() throws Exception {
        if(serviceLocator == null) return;
        System.out.println("[NEW USER REGISTRATION]");
        @NotNull final IUserService userService = serviceLocator.getUserService();
        System.out.println("[ENTER LOGIN FOR NEW ADMIN]");
        @NotNull final String login = serviceLocator.getTerminalService().nextLine();
        userService.isCorrectLogin(login);
        System.out.println("[ENTER PASSWORD FOR NEW ADMIN]");
        @NotNull final String pass = serviceLocator.getTerminalService().nextLine();
        userService.isCorrectPass(pass);
        userService.createNewUser(login, pass, UserRoleType.ADMIN);
        System.out.println("NEW ADMIN CREATED SUCCESSFULLY");
    }
}
