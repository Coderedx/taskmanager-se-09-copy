package com.shumov.tm.command.help;

import com.jcabi.manifests.Manifests;
import com.shumov.tm.command.AbstractCommand;
import com.shumov.tm.enumerate.UserRoleType;
import org.jetbrains.annotations.NotNull;

public class AboutCommand extends AbstractCommand {

    @Override
    @NotNull
    public String command() {
        return "about";
    }

    @Override
    @NotNull
    public String getDescription() {
        return "About Task Manager";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[ABOUT]");
        System.out.println("Version of build: " + Manifests.read("BuildNumber"));
        System.out.println("Built by: " + Manifests.read("Built-By"));
    }

    @Override
    public void initRoles() {
        roleTypes.add(UserRoleType.ADMIN);
        roleTypes.add(UserRoleType.USER);
        roleTypes.add(UserRoleType.GUEST);
    }
}
