package com.shumov.tm.entity;

import com.shumov.tm.api.entity.Entity;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;

import java.util.Date;
import java.util.UUID;

@Setter
@Getter
@NoArgsConstructor
public class AbstractEntity implements Entity {

    @NotNull
    protected String ownerId = "";
    @NotNull
    protected String id = UUID.randomUUID().toString();
    @NotNull
    protected Date dateCreated = new Date();

}
