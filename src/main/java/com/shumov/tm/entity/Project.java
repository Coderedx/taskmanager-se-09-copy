package com.shumov.tm.entity;



import com.shumov.tm.api.entity.Entity;
import com.shumov.tm.enumerate.ExecutionStatus;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;

import java.util.*;

@Setter
@Getter
@NoArgsConstructor
public class Project extends AbstractEntity implements Entity {
    @NotNull
    private String name = "";
    @NotNull
    private String description = "";
    @NotNull
    private Date dateStart = new Date();
    @NotNull
    private Date dateFinish = new Date();
    @NotNull
    private ExecutionStatus status = ExecutionStatus.PLANNED;

    public Project(@NotNull final String name, @NotNull final String ownerId)
    {
        this.name = name;
        this.ownerId = ownerId;
    }
}
