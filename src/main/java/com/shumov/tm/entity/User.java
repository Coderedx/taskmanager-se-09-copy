package com.shumov.tm.entity;

import com.shumov.tm.api.entity.Entity;
import com.shumov.tm.enumerate.UserRoleType;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

@Setter
@Getter
@NoArgsConstructor
public class User extends AbstractEntity implements Entity {

    @Nullable
    private String login;
    @Nullable
    private String password;
    @NotNull
    private UserRoleType userRoleType = UserRoleType.GUEST;
    @Nullable
    private String description;

    public User(@NotNull final String login, @NotNull final String password)
    {
        this.login = login;
        this.password = password;
        this.userRoleType = UserRoleType.USER;
    }

    public User(
        @NotNull final String login,
        @NotNull final String password,
        @NotNull UserRoleType userRoleType)
    {
        this.login = login;
        this.password = password;
        this.userRoleType = userRoleType;
    }
}
