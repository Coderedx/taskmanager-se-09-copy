package com.shumov.tm.enumerate;

import org.jetbrains.annotations.NotNull;

public enum UserRoleType {

    ADMIN("Administrator"),
    USER("User"),
    GUEST("Guest");

    @NotNull
    String displayName;

    UserRoleType(@NotNull String displayName){
        this.displayName = displayName;
    }

    @Override
    @NotNull
    public String toString() {
        return displayName;
    }
}
