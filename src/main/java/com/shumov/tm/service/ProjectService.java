package com.shumov.tm.service;

import com.shumov.tm.api.repository.IRepository;
import com.shumov.tm.api.service.IProjectService;
import com.shumov.tm.api.service.ITaskService;
import com.shumov.tm.entity.Project;
import com.shumov.tm.entity.Task;
import com.shumov.tm.repository.ProjectRepository;
import com.shumov.tm.enumerate.ExecutionStatus;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.IOException;
import java.util.Date;
import java.util.List;

@NoArgsConstructor
public class ProjectService extends AbstractService implements IProjectService {

    @NotNull
    protected IRepository<Project> repository = new ProjectRepository();

    public ProjectService(@NotNull final IRepository<Project> repository) {
        this.repository = repository;
    }

    @Override
    public void createProject(@Nullable final String ownerId, @Nullable final String name) throws Exception  {
        isCorrectInputData(ownerId);
        isCorrectInputData(name);
        @NotNull final Project project = new Project(name, ownerId);
        repository.persist(project);
    }

    @Override
    @NotNull
    public List<Project> getProjectList() throws Exception {
        return repository.findAll();
    }

    @Override
    @NotNull
    public List<Project> getProjectList(@Nullable final String ownerId) throws Exception {
        isCorrectInputData(ownerId);
        return repository.findAll(ownerId);
    }

    @Override
    @NotNull
    public Project getProject(@Nullable final String id) throws Exception {
        isCorrectInputData(id);
        return repository.findOne(id);
    }

    @Override
    @NotNull
    public final Project getProject(@Nullable final String ownerId, @Nullable final String id) throws Exception {
        isCorrectInputData(ownerId);
        isCorrectInputData(id);
        return repository.findOne(ownerId, id);
    }

    @Override
    public final void editProjectNameById(@Nullable final String id, @Nullable final String name) throws Exception {
        isCorrectInputData(id);
        isCorrectInputData(name);
        @NotNull final Project project = repository.findOne(id);
        project.setName(name);
        repository.merge(id, project);
    }

    @Override
    public final void editProjectNameById(
        @Nullable final String ownerId,
        @Nullable final String id,
        @Nullable final String name
    ) throws Exception {
        isCorrectInputData(ownerId);
        isCorrectInputData(id);
        isCorrectInputData(name);
        @NotNull final Project project = repository.findOne(ownerId, id);
        project.setName(name);
        repository.merge(id, project);
    }

    @Override
    public void editProjectDate(
        @Nullable String ownerId,
        @Nullable String id,
        @Nullable String start,
        @Nullable String finish
    ) throws Exception {
        isCorrectInputData(ownerId);
        isCorrectInputData(id);
        @NotNull final Date dateStart = parseDate(start);
        @NotNull final Date dateFinish = parseDate(finish);
        @NotNull final Project project = repository.findOne(ownerId, id);
        project.setDateStart(dateStart);
        project.setDateFinish(dateFinish);
        repository.merge(id, project);
    }

    @Override
    public void editProjectStatus(
        @Nullable String ownerId,
        @Nullable String id,
        @NotNull ExecutionStatus status
    ) throws Exception {
        isCorrectInputData(ownerId);
        isCorrectInputData(id);
        @NotNull final Project project = repository.findOne(ownerId, id);
        project.setStatus(status);
        repository.merge(id, project);
    }

    @Override
    public final void removeProjectById(
        @Nullable final String id,
        @NotNull final ITaskService taskService
    ) throws Exception {
        isCorrectInputData(id);
        repository.remove(id);
        @NotNull final List<Task> list = taskService.getTaskList();
        for (@NotNull final Task task : list) {
            @NotNull final String taskProjectId = task.getIdProject();
            if(!list.isEmpty() && id.equals(taskProjectId)) {
                @NotNull final String taskId = task.getId();
                taskService.removeTaskById(taskId);
            }
        }
    }

    @Override
    public final void removeProjectById(
        @Nullable final String ownerId,
        @Nullable final String id,
        @NotNull final ITaskService taskService
    ) throws Exception {
        isCorrectInputData(ownerId);
        isCorrectInputData(id);
        repository.remove(ownerId, id);
        @NotNull final List<Task> list = taskService.getTaskList(ownerId);
        for (@NotNull final Task task : list) {
            @NotNull final String taskProjectId = task.getIdProject();
            if(id.equals(taskProjectId)){
                @NotNull final String taskId = task.getId();
                taskService.removeTaskById(ownerId, taskId);
            }
        }
    }

    @Override
    public final void clearData(@NotNull final ITaskService taskService) throws Exception {
        repository.removeAll();
        taskService.clearData();
    }

    @Override
    public final void clearData(
        @Nullable final String ownerId,
        @NotNull final ITaskService taskService
    ) throws Exception {
        isCorrectInputData(ownerId);
        repository.removeAll(ownerId);
        taskService.clearData(ownerId);
    }

    @NotNull
    @Override
    public ExecutionStatus projectStatus(@NotNull final String status) throws Exception  {
        switch (status.toLowerCase()){
            case "planned" : return ExecutionStatus.PLANNED;
            case "progress" : return ExecutionStatus.IN_PROGRESS;
            case "done" : return ExecutionStatus.DONE;
            default:
                throw new IOException("[you have not selected a status]".toUpperCase());
        }
    }
}
